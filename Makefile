install: check-path install-ln

check-path:
	echo $(PATH) | grep $(HOME)/.local/bin

install-ln:
	ln -s ./sshproxy $(HOME)/.local/bin/sshproxy

install-cp:
	cp -f ./sshproxy $(HOME)/.local/bin/sshproxy
