Small scripts for creating SSH socket proxy and open browser for using it.

## Usage

## Chromium


	./sshproxy yourtargetsshserver.example.com 12345 'chromium-browser --temp-profile --proxy-server="socks5://localhost:12345"'


## Firefox

I recommend to create a Firefox profile first and configure SSH proxy on it.

	./sshproxy yourtargetsshserver.example.com 12345 'firefox -P sshproxy --no-remote'
